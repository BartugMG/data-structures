#pragma once

#include <cstddef>
#include <memory>

/// NO <bool> specialization

namespace DataStructures
{

template <typename T>
class ArrayList
{
public:
    using value_type = T;
    using size_type = size_t;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = T*;
    using const_pointer = const pointer;

    ArrayList() noexcept;
    explicit ArrayList(size_type size);
    ArrayList(size_type size, const T& initializer);
    ArrayList(std::initializer_list<T> iList);
    ArrayList(const ArrayList& other);
    ArrayList(ArrayList&& other) noexcept;

    ArrayList& operator=(const ArrayList& other);
    ArrayList& operator=(ArrayList&& other) noexcept;
    ArrayList& operator=(std::initializer_list<T> iList);

    void assign(size_type size, const T& value);
    void assign(std::initializer_list<T> iList);

    T* data() noexcept;
    const T* data() const noexcept;

    reference at(size_type pos);
    const_reference at(size_type pos) const;

    reference operator[](size_type pos);
    const_reference operator[](size_type pos) const;

    reference front();
    const_reference front() const;

    reference back();
    const_reference back() const;

    bool empty() const noexcept;
    size_type size() const noexcept;
    void reserve(size_type newSize);
    size_type capacity() const noexcept;
    void shrink_to_fit();

    void clear() noexcept;
    void insert(size_type pos, const T& value);
    void erase(size_type pos);
    void push_back(const T& value);
    void pop_back();
    void resize(size_type count);
    void resize(size_type count, const T& value);
    void swap(ArrayList& other);

    bool contains(const T& value) const noexcept;

private:
    size_type m_size;
    size_type m_capacity;
    std::unique_ptr<T[]> m_data;

    void resizeDataPtr();

    inline static char const *OUT_OF_BOUND_ERROR = "ArrayList index out of bound.";
};

template <typename T>
ArrayList<T>::ArrayList() noexcept:
    m_size(0),
    m_capacity(0)
{}

template <typename T>
ArrayList<T>::ArrayList(ArrayList::size_type size):
    m_size(size),
    m_capacity(size),
    m_data(new T[size]())
{}

template <typename T>
ArrayList<T>::ArrayList(ArrayList::size_type size, const T& initializer):
    m_size(size),
    m_capacity(size),
    m_data(new T[size]())
{
    for (size_t i = 0; i < m_size; ++i)
    {
        m_data[i] = initializer;
    }
}

template <typename T>
ArrayList<T>::ArrayList(std::initializer_list<T> iList):
    m_size(iList.size()),
    m_capacity(iList.size()),
    m_data(std::make_unique<T[]>(m_capacity))
{
    size_type index = 0;
    for (const auto& elem: iList)
    {
       m_data[index++] = elem;
    }
}

template <typename T>
ArrayList<T>::ArrayList(const ArrayList& other):
    m_size(other.m_size),
    m_capacity(other.m_capacity),
    m_data(new T[m_capacity]())
{
    for (size_type i = 0; i < m_size; ++i)
    {
        m_data[i] = other.m_data[i];
    }
}

template <typename T>
ArrayList<T>::ArrayList(ArrayList&& other) noexcept:
    m_size(std::move(other.m_size)),
    m_capacity(std::move(other.m_capacity)),
    m_data(std::move(other.m_data))
{
}

template <typename T>
ArrayList<T>& ArrayList<T>::operator=(const ArrayList& other)
{
    m_size = other.m_size;
    m_capacity = other.m_capacity;
    m_data.reset(new T[m_capacity]());

    for (size_type i = 0; i < m_size; ++i)
    {
        m_data[i] = other.m_data[i];
    }

    return *this;
}

template <typename T>
ArrayList<T>& ArrayList<T>::operator=(ArrayList&& other) noexcept
{
    m_size = std::move(other.m_size);
    m_capacity = std::move(other.m_capacity);
    m_data = std::move(other.m_data);

    return *this;
}

template <typename T>
ArrayList<T>& ArrayList<T>::operator=(std::initializer_list<T> iList)
{
    m_size = iList.size();
    m_capacity = iList.size();
    m_data.reset(new T[m_capacity]());

    size_type index = 0;
    for (const auto& elem : iList)
    {
        m_data[index] = elem;
        ++index;
    }

    return *this;
}

template <typename T>
void ArrayList<T>::assign(ArrayList::size_type size, const T& value)
{
    m_size = size;
    m_capacity = size;
    m_data.reset(new T[m_capacity]());

    for (size_t i = 0; i < m_size; ++i)
    {
        m_data[i] = value;
    }
}

template <typename T>
void ArrayList<T>::assign(std::initializer_list<T> iList)
{
    *this = iList;
}

template <typename T>
bool ArrayList<T>::empty() const noexcept
{
    return m_size == 0;
}

template <typename T>
typename ArrayList<T>::size_type ArrayList<T>::size() const noexcept
{
    return m_size;
}

template <typename T>
typename ArrayList<T>::size_type ArrayList<T>::capacity() const noexcept
{
    return m_capacity;
}

template <typename T>
T* ArrayList<T>::data() noexcept
{
    return m_data.get();
}

template <typename T>
const T* ArrayList<T>::data() const noexcept
{
    return m_data.get();
}

template <typename T>
typename ArrayList<T>::reference ArrayList<T>::at(ArrayList::size_type pos)
{
    if (pos >= size())
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }
    return m_data[pos];
}

template <typename T>
typename ArrayList<T>::const_reference ArrayList<T>::at(ArrayList::size_type pos) const
{
    if (pos >= size())
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }
    return m_data[pos];
}

template <typename T>
typename ArrayList<T>::reference ArrayList<T>::operator[](ArrayList::size_type pos)
{
    return m_data[pos];
}

template <typename T>
typename ArrayList<T>::const_reference ArrayList<T>::operator[](ArrayList::size_type pos) const
{
    return m_data[pos];
}

template <typename T>
typename ArrayList<T>::reference ArrayList<T>::front()
{
    return m_data[0];
}

template <typename T>
typename ArrayList<T>::const_reference ArrayList<T>::front() const
{
    return m_data[0];
}

template <typename T>
typename ArrayList<T>::reference ArrayList<T>::back()
{
    return m_data[m_size-1];
}

template <typename T>
typename ArrayList<T>::const_reference ArrayList<T>::back() const
{
    return m_data[m_size-1];
}

template <typename T>
void ArrayList<T>::resizeDataPtr()
{
    auto oldPtr = std::move(m_data);
    m_data.reset(new T[m_capacity]());

    for (size_t i = 0; i < m_size; ++i)
    {
        m_data[i] = std::move(oldPtr[i]);
    }
}

template <typename T>
void ArrayList<T>::reserve(ArrayList::size_type newSize)
{
    if (newSize <= m_capacity)
    {
        return;
    }

    m_capacity = newSize;

    resizeDataPtr();
}

template <typename T>
void ArrayList<T>::shrink_to_fit()
{
    if (m_capacity == m_size)
    {
        return;
    }

    m_capacity = m_size;

    resizeDataPtr();
}

template <typename T>
void ArrayList<T>::clear() noexcept
{
    m_size = 0;
    m_data.release();
    m_data.reset(new T[m_capacity]());
}

template <typename T>
void ArrayList<T>::insert(ArrayList::size_type pos, const T& value)
{
    if (pos > m_size)
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }

    if (m_capacity == 0u)
    {
        m_capacity = 1u;
        m_data.reset(new T[m_capacity]());
    }
    else if (m_size == m_capacity)
    {
        m_capacity *= 2;
        resizeDataPtr();
    }

    if (pos != m_size)
    {
        for (size_t i = m_size; i > pos; --i)
        {
            m_data[i] = std::move(m_data[i-1]);
        }
    }

    m_data[pos] = value;
    ++m_size;
}

template <typename T>
void ArrayList<T>::push_back(const T& value)
{
    insert(m_size, value);
}

template <typename T>
void ArrayList<T>::erase(ArrayList::size_type pos)
{
    if (pos >= m_size)
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }

    if (pos != m_size-1)
    {
        for(size_type i = pos; i < m_size-1; ++i)
        {
            m_data[i] = std::move(m_data[i+1]);
        }
    }

    m_data[m_size-1] = T();

    --m_size;
}

template <typename T>
void ArrayList<T>::pop_back()
{
    erase(m_size-1);
}

template <typename T>
void ArrayList<T>::swap(ArrayList& other)
{
    std::swap(m_size, other.m_size);
    std::swap(m_capacity, other.m_capacity);
    m_data.swap(other.m_data);
}

template <typename T>
void ArrayList<T>::resize(ArrayList::size_type count, const T& value)
{
    if (count == m_size)
    {
        return;
    }

    if (count > m_size)
    {
        if (count > m_capacity)
        {
            m_capacity = count;
            resizeDataPtr();
        }

        for (size_type i = m_size; i < count; ++i)
        {
            m_data[i] = value;
        }
    }
    m_size = count;
}

template <typename T>
void ArrayList<T>::resize(ArrayList::size_type count)
{
    resize(count, T());
}

template <typename T>
bool ArrayList<T>::contains(const T& value) const noexcept
{
    for(size_type i = 0u; i < m_size; ++i)
    {
        if (m_data[i] == value)
        {
            return true;
        }
    }
    return false;
}

}

