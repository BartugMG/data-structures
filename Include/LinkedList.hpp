#pragma once

#include <cstddef>
#include <memory>
#include <stdexcept>

namespace DataStructures
{

template <typename T>
class LinkedList
{
    struct Node
    {
        Node(): data(T()) {}
        explicit Node(const T& value): data(value) {}

        T data;
        std::shared_ptr<Node> next;
    };

public:
    using value_type = T;
    using size_type = size_t;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = T*;
    using const_pointer = const pointer;

    LinkedList() noexcept;
    explicit LinkedList(size_type size);
    LinkedList(size_type size, const T& initializer);
    LinkedList(std::initializer_list<T> iList);
    LinkedList(const LinkedList& other);
    LinkedList(LinkedList&& other) noexcept;

    LinkedList& operator=(std::initializer_list<T> iList);
    LinkedList& operator=(const LinkedList& other);
    LinkedList& operator=(LinkedList&& other) noexcept;

    reference at(size_type pos);
    const_reference at(size_type pos) const;

    reference front();
    const_reference front() const;

    reference back();
    const_reference back() const;

    bool empty() const noexcept;
    size_type size() const noexcept;

    void clear() noexcept;
    void insert(size_type pos, const T& value);
    void erase(size_type pos);
    void push_back(const T& value);
    void pop_back();
    void push_front(const T& value);
    void pop_front();
    void resize(size_type count);
    void resize(size_type count, const T& value);
    void swap(LinkedList& other);

    bool contains(const T& value) const noexcept;

private:
    size_type m_size;
    std::shared_ptr<Node> m_head;
    std::shared_ptr<Node> m_tail;

    inline static char const* OUT_OF_BOUND_ERROR = "LinkedList index out of bound.";

    void copyFromInitializerList(const std::initializer_list<T>& iList);
    void deepCopyOther(const LinkedList& other);
};

template <typename T>
LinkedList<T>::LinkedList() noexcept:
        m_size(0)
{}

template <typename T>
LinkedList<T>::LinkedList(LinkedList::size_type size, const T& initializer):
        m_size(size)
{
    if (m_size == 0)
    {
        return;
    }

    m_head = std::make_shared<Node>(initializer);

    auto current = m_head;

    for (size_type i = 1; i < m_size; ++i)
    {
        current->next = std::make_shared<Node>(initializer);
        current = current->next;
    }

    m_tail = current;
}

template <typename T>
LinkedList<T>::LinkedList(LinkedList::size_type size):
        LinkedList(size, T())
{}

template <typename T>
LinkedList<T>::LinkedList(std::initializer_list<T> iList):
        m_size(iList.size())
{
    if (m_size > 0)
    {
        copyFromInitializerList(iList);
    }
}

template <typename T>
LinkedList<T>::LinkedList(const LinkedList& other):
        m_size(other.m_size)
{
    if (m_size > 0)
    {
        deepCopyOther(other);
    }
}

template <typename T>
LinkedList<T>::LinkedList(LinkedList&& other) noexcept:
        m_size(std::move(other.m_size)),
        m_head(std::move(other.m_head)),
        m_tail(std::move(other.m_tail))
{}

template <typename T>
LinkedList<T>& LinkedList<T>::operator=(std::initializer_list<T> iList)
{
    m_size = iList.size();

    if (m_size > 0)
    {
        copyFromInitializerList(iList);
    }
    else
    {
        m_head.reset();
        m_tail.reset();
    }

    return *this;
}

template <typename T>
LinkedList<T>& LinkedList<T>::operator=(const LinkedList& other)
{
    m_size = other.m_size;

    if (m_size > 0)
    {
        deepCopyOther(other);
    }
    else
    {
        m_head.reset();
        m_tail.reset();
    }

    return *this;
}

template <typename T>
LinkedList<T>& LinkedList<T>::operator=(LinkedList&& other) noexcept
{
    m_size = std::move(other.m_size);
    m_head = std::move(other.m_head);
    m_tail = std::move(other.m_tail);

    return *this;
}

template <typename T>
void LinkedList<T>::copyFromInitializerList(const std::initializer_list<T>& iList)
{
    m_head = std::make_shared<Node>(*(iList.begin()));

    auto current = m_head;
    for (auto it = std::next(iList.begin()); it != iList.end(); ++it)
    {
        current->next = std::make_shared<Node>(*it);
        current = current->next;
    }

    m_tail = current;
}

template <typename T>
void LinkedList<T>::deepCopyOther(const LinkedList& other)
{
    m_head = std::make_shared<Node>(other.m_head->data);

    auto current = m_head;
    auto otherCurrent = other.m_head;

    while (otherCurrent->next)
    {
        current->next = std::make_shared<Node>(otherCurrent->next->data);

        current = current->next;
        otherCurrent = otherCurrent->next;
    }


    m_tail = current;
}

template <typename T>
typename LinkedList<T>::reference LinkedList<T>::front()
{
    return m_head->data;
}

template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::front() const
{
    return m_head->data;
}

template <typename T>
typename LinkedList<T>::reference LinkedList<T>::back()
{
    return m_tail->data;
}

template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::back() const
{
    return m_tail->data;
}

template <typename T>
bool LinkedList<T>::empty() const noexcept
{
    return m_size == 0;
}

template <typename T>
typename LinkedList<T>::size_type LinkedList<T>::size() const noexcept
{
    return m_size;
}

template <typename T>
typename LinkedList<T>::reference LinkedList<T>::at(LinkedList::size_type pos)
{
    return const_cast<reference>(const_cast<const LinkedList*>(this)->at(pos));
}

template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::at(LinkedList::size_type pos) const
{
    if (pos >= m_size)
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }

    if (pos == m_size-1)
    {
        return m_tail->data;
    }

    auto current = m_head;

    for (size_type i = 0; i < pos; ++i)
    {
        current = current->next;
    }

    return current->data;
}

template <typename T>
bool LinkedList<T>::contains(const T& value) const noexcept
{
    if (m_size == 0)
    {
        return false;
    }

    auto current = m_head;

    while (current->next)
    {
       if (current->data == value)
       {
           return true;
       }
       current = current->next;
    }

    return false;
}

template <typename T>
void LinkedList<T>::clear() noexcept
{
    m_size = 0;
    m_head.reset();
    m_tail.reset();
}

template <typename T>
void LinkedList<T>::insert(LinkedList::size_type pos, const T& value)
{
    if (pos > m_size)
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }

    auto newNode = std::make_shared<Node>(value);

    if (pos == 0)
    {
        newNode->next = m_head;
        m_head = newNode;
        if (m_size == 0)
        {
            m_tail = m_head;
        }
        ++m_size;
        return;
    }

    if (pos == m_size)
    {
        m_tail->next = newNode;
        m_tail = newNode;
        ++m_size;
        return;
    }

    auto current = m_head;

    for (size_type i = 1; i < pos; ++i)
    {
        current = current->next;
    }

    newNode->next = current->next;
    current->next = newNode;
    ++m_size;
}

template <typename T>
void LinkedList<T>::erase(LinkedList::size_type pos)
{
    if (pos >= m_size)
    {
        throw std::out_of_range(OUT_OF_BOUND_ERROR);
    }

    if (m_size == 1)
    {
        m_head.reset();
        m_tail.reset();
        --m_size;
        return;
    }

    if (pos == 0)
    {
        m_head = m_head->next;
        --m_size;
        if (m_size == 1)
        {
            m_tail = m_head;
        }
        return;
    }

    auto current = m_head;


    for (size_type i = 1; i < pos; ++i)
    {
        current = current->next;
    }
    current->next = current->next->next;

    if (pos == m_size-1)
    {
        m_tail = current;
    }

    --m_size;
}

template <typename T>
void LinkedList<T>::push_back(const T& value)
{
    insert(m_size, value);
}

template <typename T>
void LinkedList<T>::pop_back()
{
    if (m_size == 0)
    {
        throw std::out_of_range("No elements left to remove");
    }
    erase(m_size-1);
}

template <typename T>
void LinkedList<T>::push_front(const T& value)
{
    insert(0u, value);
}

template <typename T>
void LinkedList<T>::pop_front()
{
    if (m_size == 0)
    {
        throw std::out_of_range("No elements left to remove");
    }
    erase(0u);

}

template <typename T>
void LinkedList<T>::resize(LinkedList::size_type count)
{
    resize(count, T());
}

template <typename T>
void LinkedList<T>::resize(LinkedList::size_type count, const T& value)
{
    while (m_size > count)
    {
        pop_back();
    }
    while (m_size < count)
    {
        push_back(value);
    }

}

template <typename T>
void LinkedList<T>::swap(LinkedList& other)
{
    std::swap(m_size, other.m_size);
    std::swap(m_head, other.m_size);
    std::swap(m_tail, other.m_tail);
}


}
